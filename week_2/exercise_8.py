def find_divisible_subset(number_from, number_to, divisible_by):
     divisible_numbers = [x for x in range(number_from,number_to) if x% divisible_by==0 ]
     return divisible_numbers

if __name__ == '__main__':
    result_from_divisible_subset = find_divisible_subset(1,25,3)
    print(result_from_divisible_subset)