def get_player_description(player):
    player_description = f"The player {player['nick']} is of type {player['type']} and has {player['exp_points']} EXP"
    print(player_description)

if __name__ == '__main__':
    player = {
        "nick": "maestro_54",
        "type": "warrior",
        "exp_points": 3000
    }
    get_player_description(player)

