emails = ["a@example.com", "b@example.com"]

if __name__ == '__main__':
    print(len(emails))
    print("First index: ", emails[0])
    print("Last index", emails[-1])
    emails.append("cde@example.com")
    print(emails)

