def searching_for_a_number(list_numbers):
    long_list_of_numbers = len(list_numbers) // 2
    # print(long_list_of_numbers)

    if len(list_numbers) % 2 == 1:
        return list_numbers[long_list_of_numbers]
    else:
        return list_numbers[long_list_of_numbers - 1]


if __name__ == '__main__':
     first_list_of_numbers = [10, 3, 4, 11, 90]
     second_list_of_numbers = [10, 3, 4, 11]
     result_first_list_of_numbers = searching_for_a_number(first_list_of_numbers)
     result_second_list_of_numbers = searching_for_a_number(second_list_of_numbers)
     print(result_first_list_of_numbers)
     print(result_second_list_of_numbers)