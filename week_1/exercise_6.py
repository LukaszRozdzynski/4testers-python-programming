
def temperature_in_celsius_to_fahrenheit(temp_celsius_list):
    return 9 / 5 * temp_celsius_list + 32

def get_list_of_temperatures_in_celsius_and_return_list_in_farenhait(temps_in_celsius):
    temperature_fahrenheit = [temperature_in_celsius_to_fahrenheit(temp) for temp in temps_in_celsius]
    return temperature_fahrenheit

temps_in_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
score = get_list_of_temperatures_in_celsius_and_return_list_in_farenhait(temps_in_celsius)
print(score)