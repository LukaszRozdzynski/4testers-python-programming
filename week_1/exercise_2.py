def email_address_generator(name,surname):
    return f"{name}.{surname}@testers.pl"

if __name__ == '__main__':
    email_address_of_user_man = email_address_generator("Janusz","Nowak")
    email_address_of_user_woman = email_address_generator("Barbara","Kowalska")
    print(email_address_of_user_man)
    print(email_address_of_user_woman)