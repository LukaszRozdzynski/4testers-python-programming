import random
from datetime import datetime

# Listy zawierające dane do losowania
female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']



def calculation_of_the_age_of_a_person(age):
    current_date = datetime.now()
    current_year = current_date.year
    return current_year - age


list_of_people = []
def generate_people_of_the_list():
    for i in range(5):


        random_female_firstname = random.choice(female_fnames)
        random_male_firstname = random.choice(male_fnames)
        random_surname = random.choice(surnames)
        random_age = random.randint(5, 65)
        random_country = random.choice(countries)
        random_female_email = f"{random_female_firstname.lower()}.{random_surname.lower()}@example.com"
        random_male_email = f"{random_male_firstname.lower()}.{random_surname.lower()}@example.com"
        adult = random_age >= 18
        birthday_year = calculation_of_the_age_of_a_person(random_age)

        female_list = {
            "firstname": random_female_firstname,
            "lastname": random_surname,
            "country": random_country,
            "email": random_female_email,
            "age": random_age,
            "adult": adult,
            "birth_year": birthday_year
        }

        male_list = {
            "firstname": random_male_firstname,
            "lastname": random_surname,
            "country": random_country,
            "email": random_male_email,
            "age": random_age,
            "adult": adult,
            "birth_year": birthday_year,
        }

        list_of_people.extend([female_list,male_list])
        print(list_of_people)

generate_people_of_the_list()
def generate_description():
    for person_from_the_list in list_of_people:
       description = f" Hi! I'm {person_from_the_list['firstname']}. I come from {person_from_the_list['country']} and I was born in {person_from_the_list['birth_year']}"
       print(description)

generate_description()